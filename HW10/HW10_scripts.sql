sp_configure 'show advanced options', 1;  
GO  
RECONFIGURE;  
GO  
sp_configure 'clr enabled', 1;  
GO  
RECONFIGURE;  
GO
sp_configure 'clr strict security', 0;  
GO  
RECONFIGURE;  
GO

-- dll - ����� �� ������� � ������ � ������������
CREATE ASSEMBLY CLRFunctions FROM 'C:\Users\bokkaradm\Desktop\otus-mssql-2020-09-sergey_b\HW10\OTUS_CLR_test.dll'  
GO 

CREATE FUNCTION [dbo].fnSplitStringCLR(@text [nvarchar](max), @delimiter [nchar](1))
RETURNS TABLE (
part nvarchar(max),
ID_ODER int
) WITH EXECUTE AS CALLER
AS
EXTERNAL NAME CLRFunctions.[OTUS_CLR_test2.UserDefinedFunctions].SplitString

GO

SELECT *
FROM [dbo].[fnSplitStringCLR]('mama,mula,ramy,mulom', ',')