USE [WideWorldImporters]
GO 
/*
������������ PIVOT
����� ������������ PIVOT.
�� ������� �� ������� ���������� CROSS APPLY, PIVOT, CUBE�.
��������� �������� ������, ������� � ���������� ������ ���������� ��������� ������� ���������� ����:
�������� �������
�������� ���������� �������

����� �������� ������, ������� ����� ������������ ���������� ��� ���� ��������.
��� ������� ��������� ��������� �� CustomerName.
*/

DECLARE 
@ColunmName nvarchar(max),
@dml nvarchar(max)

SELECT @ColunmName = ISNULL(@ColunmName + ', ', '') + QUOTENAME(Names)
			FROM (SELECT CustomerName as Names
			FROM [Sales].[Customers]) as N

SET @dml = 
		N'SELECT * 
		FROM (
			SELECT 
				CustomerName
				,FORMAT(o.OrderDate, ''MM.yy'') as OrderDate
				,o.OrderID
			FROM [Sales].[Customers] cus
			LEFT JOIN [Sales].[Orders] o on o.CustomerID = cus.CustomerID
		) as A
		PIVOT (
			COUNT(OrderId)
			FOR CustomerName IN (' + @ColunmName + ')
		) as PVT
		ORDER BY OrderDate;';
	
exec sp_executesql @dml 

