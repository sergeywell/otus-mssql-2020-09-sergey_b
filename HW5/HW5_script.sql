/* 
1. �������� ������ � ��������� �������� � ���������� ��� � ��������� ����������. �������� �����.
� �������� ������� � ��������� �������� � ��������� ���������� ����� ����� ���� ������ ��� ��������� ������:
������� ������ ����� ������ ����������� ������ �� ������� � 2015 ���� (� ������ ������ ������ �� ����� ����������, 
��������� ����� � ������� ������� �������)
�������� id �������, �������� �������, ���� �������, ����� �������, ����� ����������� ������
������
���� ������� ����������� ���� �� ������
2015-01-29 4801725.31
2015-01-30 4801725.31
2015-01-31 4801725.31
2015-02-01 9626342.98
2015-02-02 9626342.98
2015-02-03 9626342.98
������� ����� ����� �� ������� Invoices.
����������� ���� ������ ���� ��� ������� �������.

�������� 2 �������� ������� - ����� windows function � ��� ���.
�������� ����� ������� �����������, �������� �� set statistics time on;
*/

SELECT i.InvoiceID, c.CustomerName, i.InvoiceDate, SUM(il.Quantity*il.UnitPrice) as TotanByInvoice
INTO #tmp_salese
FROM [Sales].[Invoices] i
LEFT JOIN [Sales].[InvoiceLines] il on i.InvoiceID = il.InvoiceID
LEFT JOIN [Sales].[Customers] c on i.CustomerID = c.CustomerID
WHERE i.InvoiceDate > '20150101'
GROUP BY i.InvoiceID, c.CustomerName, i.InvoiceDate

SELECT InvoiceID, CustomerName, InvoiceDate, TotanByInvoice,
		SUM(TotanByInvoice) OVER(ORDER BY YEAR(InvoiceDate), MONTH(InvoiceDate)) as TotalByMonth
FROM #tmp_salese

DROP TABLE #tmp_salese;

-----------------------------

DECLARE @tmp_sales TABLE (InvoiceID INT, CustomerName nvarchar(100), 
					InvoiceDate date, TotanByInvoice decimal(18,2))

INSERT INTO @tmp_sales 
SELECT i.InvoiceID, c.CustomerName, i.InvoiceDate, SUM(il.Quantity*il.UnitPrice) as TotanByInvoice
FROM [Sales].[Invoices] i
LEFT JOIN [Sales].[InvoiceLines] il on i.InvoiceID = il.InvoiceID
LEFT JOIN [Sales].[Customers] c on i.CustomerID = c.CustomerID
WHERE i.InvoiceDate > '20150101'
GROUP BY i.InvoiceID, c.CustomerName, i.InvoiceDate
 
SELECT InvoiceID, CustomerName, InvoiceDate, TotanByInvoice,
		SUM(TotanByInvoice) OVER(ORDER BY YEAR(InvoiceDate), MONTH(InvoiceDate)) as TotalByMonth
FROM @tmp_sales; 


/*
2. ������� ������ 2� ����� ���������� ��������� (�� ���-�� ���������) � ������ ������ �� 2016� ��� 
(�� 2 ����� ���������� �������� � ������ ������)
*/
WITH A AS(
	SELECT MONTH(i.InvoiceDate) [Month], si.StockItemName, SUM(il.Quantity) as Qty
	FROM [Sales].[Invoices] i
	LEFT JOIN [Sales].[InvoiceLines] il on i.InvoiceID = il.InvoiceID
	LEFT JOIN [Warehouse].[StockItems] si on il.StockItemID=si.StockItemID
	WHERE i.InvoiceDate BETWEEN '20160101' AND '20161231'
	GROUP BY  MONTH(i.InvoiceDate), si.StockItemName),
B AS (
	SELECT *, 
	ROW_NUMBER() OVER(PARTITION BY Month ORDER BY Qty desc) as rnk
	FROM A)
SELECT Month, StockItemName, Qty 
FROM B
WHERE rnk in (1, 2)
ORDER BY Month;


/* 
3. ������� ����� ��������
���������� �� ������� �������, � ����� ����� ������ ������� �� ������, ��������, ����� � ����
������������ ������ �� �������� ������, ��� ����� ��� ��������� ����� �������� ��������� ���������� ������
���������� ����� ���������� ������� � �������� ����� � ���� �� �������
���������� ����� ���������� ������� � ����������� �� ������ ����� �������� ������
���������� ��������� id ������ ������ �� ����, ��� ������� ����������� ������� �� �����
���������� �� ������ � ��� �� �������� ����������� (�� �����)
�������� ������ 2 ������ �����, � ������ ���� ���������� ������ ��� ����� ������� "No items"
����������� 30 ����� ������� �� ���� ��� ������ �� 1 ��
--��� ���� ������ �� ����� ������ ������ ��� ������������� �������
*/

SELECT StockItemID, StockItemName, 
		isnull(Brand, 'UNKNOWN') as Brand,
		UnitPrice,
		ROW_NUMBER() OVER(PARTITION BY LEFT(StockItemName, 1) ORDER BY StockItemName) as NumByFirstLet,
		COUNT(*) OVER() as TotalCount,
		COUNT(*) OVER (PARTITION BY LEFT(StockItemName, 1)) as CountByFirstLet,
		LEAD(StockItemID) OVER(ORDER BY StockItemName) as NextItemID,
		LAG(StockItemID) OVER(ORDER BY StockItemName) as PrevItemId,
		ISNULL(LAG(StockItemName, 2) OVER(ORDER BY StockItemName), 'No items') as PrevName2rows,
		NTILE(30) OVER (ORDER BY TypicalWeightPerUnit) as Ntile30
FROM [Warehouse].[StockItems];


/* 
4. �� ������� ���������� �������� ���������� �������, �������� ��������� ���-�� ������
� ����������� ������ ���� �� � ������� ����������, �� � �������� �������, ���� �������, ����� ������
*/

WITH A as (
	SELECT i.SalespersonPersonID, p.FullName, i.CustomerID, c.CustomerName, i.InvoiceDate, 
		  SUM(il.UnitPrice*il.Quantity) as TotalSum, i.InvoiceID
	FROM [Sales].[Invoices] i
	LEFT JOIN [Sales].[InvoiceLines] il on i.InvoiceID = il.InvoiceID
	LEFT JOIN [Sales].[Customers] c on i.CustomerID = c.CustomerID
	LEFT JOIN [Application].[People] p on i.SalespersonPersonID = p.PersonID
	GROUP BY  i.SalespersonPersonID, p.FullName, i.CustomerID, c.CustomerName, i.InvoiceDate, i.InvoiceId),
B as (
	SELECT SalespersonPersonID, FullName, CustomerID, CustomerName, InvoiceDate, TotalSum,
		ROW_NUMBER() OVER(PARTITION BY SalespersonPersonID ORDER BY InvoiceDAte DESC, InvoiceID DESC) as rnk
	FROM A)
SELECT SalespersonPersonID, FullName, CustomerID, CustomerName, InvoiceDate, TotalSum 
FROM B
WHERE rnk = 1;


/* 
5. �������� �� ������� ������� 2 ����� ������� ������, ������� �� �������
� ����������� ������ ���� �� ������, ��� ��������, �� ������, ����, ���� �������
*/

WITH A as (
	SELECT i.CustomerID, c.CustomerName, il.StockItemID, 
			il.UnitPrice, i.InvoiceDate,
			DENSE_RANK() OVER(PARTITION BY i.CustomerID ORDER BY il.UnitPrice desc) as rnk
	FROM [Sales].[Invoices] i
	LEFT JOIN [Sales].[InvoiceLines] il on i.InvoiceID = il.InvoiceID
	LEFT JOIN [Sales].[Customers] c on i.CustomerID = c.CustomerID)
SELECT CustomerID, CustomerName, StockItemID, 
		UnitPrice, InvoiceDate
FROM A
WHERE rnk in (1, 2)
ORDER BY CustomerID;


/*
����������� ����� ������� ������� �������� ��� ������� 2,4,5 ��� ������������� windows function � �������� �������� ��� � ������� 1.

Bonus �� ���������� ����
�������� ������, ������� �������� 10 ��������, ������� ������� ������ 30 ������� � ��������� ����� 
��� �� ������� ������ 2016.
*/
WITH A as (
	SELECT i.CustomerID, c.CustomerName,
			COUNT(i.InvoiceID) OVER(PARTITION BY i.CustomerID) as InvoiceCount,
			ROW_NUMBER() OVER (PARTITION BY i.CustomerID ORDER BY InvoiceDate DESC) as rnk
	FROM [Sales].[Invoices] i
	LEFT JOIN [Sales].[InvoiceLines] il on i.InvoiceID = il.InvoiceID
	LEFT JOIN [Sales].[Customers] c on i.CustomerID = c.CustomerID
	WHERE i.InvoiceDate < '20160501')
SELECT TOP 10 CustomerID, CustomerName, InvoiceCount
FROM A
WHERE rnk = 1
ORDER BY InvoiceCount DESC;