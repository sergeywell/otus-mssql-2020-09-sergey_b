--1. ����������� � ���� 5 ������� ��������� insert � ������� Customers ��� Suppliers
INSERT INTO [Sales].[Customers]
	([CustomerID]
      ,[CustomerName]
      ,[BillToCustomerID]
      ,[CustomerCategoryID]
      ,[BuyingGroupID]
      ,[PrimaryContactPersonID]
      ,[AlternateContactPersonID]
      ,[DeliveryMethodID]
      ,[DeliveryCityID]
      ,[PostalCityID]
      ,[CreditLimit]
      ,[AccountOpenedDate]
      ,[StandardDiscountPercentage]
      ,[IsStatementSent]
      ,[IsOnCreditHold]
      ,[PaymentDays]
      ,[PhoneNumber]
      ,[FaxNumber]
      ,[DeliveryRun]
      ,[RunPosition]
      ,[WebsiteURL]
      ,[DeliveryAddressLine1]
      ,[DeliveryAddressLine2]
      ,[DeliveryPostalCode]
      ,[DeliveryLocation]
      ,[PostalAddressLine1]
      ,[PostalAddressLine2]
      ,[PostalPostalCode]
      ,[LastEditedBy])
VALUES 
	(NEXT VALUE FOR [WideWorldImporters].[Sequences].[CustomerID], 'Nasha Ryaba 1',1,3,1,1001,1002,3,19586,19586,NULL,'2013-01-01',0.000,0,0,7,'(308) 555-0100','(308) 555-0101','','','http://www.tailspintoys.com','Shop 38','1877 Mittal Road','90410',0xE6100000010CE73F5A52A4BF444010638852B1A759C0,'PO Box 8975','Ribeiroville','90410',1),
	(NEXT VALUE FOR [WideWorldImporters].[Sequences].[CustomerID], 'Nasha Ryaba 2',1,3,1,1001,1002,3,19586,19586,NULL,'2013-01-01',0.000,0,0,7,'(308) 555-0100','(308) 555-0101','','','http://www.tailspintoys.com','Shop 38','1877 Mittal Road','90410',0xE6100000010CE73F5A52A4BF444010638852B1A759C0,'PO Box 8975','Ribeiroville','90410',1),
	(NEXT VALUE FOR [WideWorldImporters].[Sequences].[CustomerID], 'Nasha Ryaba 3',1,3,1,1001,1002,3,19586,19586,NULL,'2013-01-01',0.000,0,0,7,'(308) 555-0100','(308) 555-0101','','','http://www.tailspintoys.com','Shop 38','1877 Mittal Road','90410',0xE6100000010CE73F5A52A4BF444010638852B1A759C0,'PO Box 8975','Ribeiroville','90410',1),
	(NEXT VALUE FOR [WideWorldImporters].[Sequences].[CustomerID], 'Nasha Ryaba 4',1,3,1,1001,1002,3,19586,19586,NULL,'2013-01-01',0.000,0,0,7,'(308) 555-0100','(308) 555-0101','','','http://www.tailspintoys.com','Shop 38','1877 Mittal Road','90410',0xE6100000010CE73F5A52A4BF444010638852B1A759C0,'PO Box 8975','Ribeiroville','90410',1),
	(NEXT VALUE FOR [WideWorldImporters].[Sequences].[CustomerID], 'Nasha Ryaba 5',1,3,1,1001,1002,3,19586,19586,NULL,'2013-01-01',0.000,0,0,7,'(308) 555-0100','(308) 555-0101','','','http://www.tailspintoys.com','Shop 38','1877 Mittal Road','90410',0xE6100000010CE73F5A52A4BF444010638852B1A759C0,'PO Box 8975','Ribeiroville','90410',1);


--2. ������� 1 ������ �� Customers, ������� ���� ���� ���������
DELETE 
FROM [Sales].[Customers]
WHERE CustomerId = (
		SELECT TOP 1 CustomerId
		FROM [Sales].[Customers]
		ORDER BY CustomerID DESC);




--3. �������� ���� ������, �� ����������� ����� UPDATE

UPDATE [Sales].[Customers]
SET CustomerName = 'Nasha Ryaba UPD'
WHERE CustomerName like 'Nasha Ryaba 3';


--4. �������� MERGE, ������� ������� ������� ������ � �������, ���� �� ��� ���, � ������� ���� ��� ��� ����

--CREATE TABLE [Sales].[Customers_Test_MERGE](
--	[CustomerID] [int] NOT NULL,
--	[CustomerName] [nvarchar](100) NOT NULL,
--	[BillToCustomerID] [int] NOT NULL,
--	[CustomerCategoryID] [int] NOT NULL,
--	[BuyingGroupID] [int] NULL,
--	[PrimaryContactPersonID] [int] NOT NULL,
--	[AlternateContactPersonID] [int] NULL,
--	[DeliveryMethodID] [int] NOT NULL,
--	[DeliveryCityID] [int] NOT NULL,
--	[PostalCityID] [int] NOT NULL,
--	[CreditLimit] [decimal](18, 2) NULL,
--	[AccountOpenedDate] [date] NOT NULL,
--	[StandardDiscountPercentage] [decimal](18, 3) NOT NULL,
--	[IsStatementSent] [bit] NOT NULL,
--	[IsOnCreditHold] [bit] NOT NULL,
--	[PaymentDays] [int] NOT NULL,
--	[PhoneNumber] [nvarchar](20) NOT NULL,
--	[FaxNumber] [nvarchar](20) NOT NULL,
--	[DeliveryRun] [nvarchar](5) NULL,
--	[RunPosition] [nvarchar](5) NULL,
--	[WebsiteURL] [nvarchar](256) NOT NULL,
--	[DeliveryAddressLine1] [nvarchar](60) NOT NULL,
--	[DeliveryAddressLine2] [nvarchar](60) NULL,
--	[DeliveryPostalCode] [nvarchar](10) NOT NULL,
--	[DeliveryLocation] [geography] NULL,
--	[PostalAddressLine1] [nvarchar](60) NOT NULL,
--	[PostalAddressLine2] [nvarchar](60) NULL,
--	[PostalPostalCode] [nvarchar](10) NOT NULL,
--	[LastEditedBy] [int] NOT NULL,
--	[ValidFrom] [datetime2](7) NOT NULL,
--	[ValidTo] [datetime2](7) NOT NULL
--) ON [USERDATA] TEXTIMAGE_ON [USERDATA]
--GO

--INSERT INTO [Sales].[Customers_Teste_MERG]
--SELECT TOP 4 *
--  FROM [WideWorldImporters].[Sales].[Customers]
--  order by customerId desc;


MERGE [Sales].[Customers] AS T
USING [Sales].[Customers_Test_MERGE] AS S
	ON (T.CustomerID = S.CustomerID)
WHEN MATCHED 
	THEN UPDATE
		SET t.CustomerName = s.CustomerName
WHEN NOT MATCHED
	THEN INSERT ([CustomerID],[CustomerName],[BillToCustomerID],[CustomerCategoryID],[BuyingGroupID]
      ,[PrimaryContactPersonID],[AlternateContactPersonID],[DeliveryMethodID],[DeliveryCityID]
      ,[PostalCityID],[CreditLimit],[AccountOpenedDate],[StandardDiscountPercentage],[IsStatementSent]
      ,[IsOnCreditHold],[PaymentDays],[PhoneNumber],[FaxNumber],[DeliveryRun],[RunPosition]
      ,[WebsiteURL],[DeliveryAddressLine1],[DeliveryAddressLine2],[DeliveryPostalCode],[DeliveryLocation]
      ,[PostalAddressLine1],[PostalAddressLine2],[PostalPostalCode],[LastEditedBy])
	VALUES([CustomerID],[CustomerName],[BillToCustomerID],[CustomerCategoryID],[BuyingGroupID]
      ,[PrimaryContactPersonID],[AlternateContactPersonID],[DeliveryMethodID],[DeliveryCityID]
      ,[PostalCityID],[CreditLimit],[AccountOpenedDate],[StandardDiscountPercentage],[IsStatementSent]
      ,[IsOnCreditHold],[PaymentDays],[PhoneNumber],[FaxNumber],[DeliveryRun],[RunPosition]
      ,[WebsiteURL],[DeliveryAddressLine1],[DeliveryAddressLine2],[DeliveryPostalCode],[DeliveryLocation]
      ,[PostalAddressLine1],[PostalAddressLine2],[PostalPostalCode],[LastEditedBy])
OUTPUT deleted.*, $action, inserted.*;


--5. �������� ������, ������� �������� ������ ����� bcp out � ��������� ����� bulk insert

EXEC master..xp_cmdshell 'bcp "[WideWorldImporters].Sales.InvoiceLines" out  "D:\otus-mssql-2020-09-sergey_b\HW8\InvoiceLines1.txt" 
								-T -w -t, -S 127.0.0.1'

GO

BULK INSERT [WideWorldImporters].[Sales].[InvoiceLines_BulkDemo]
				   FROM "D:\otus-mssql-2020-09-sergey_b\HW8\InvoiceLines1.txt"
				   WITH 
					 (
						FIELDTERMINATOR = ',',
						ROWTERMINATOR = '\n',
						datafiletype = 'char',
						CODEPAGE = '65001'
					  );