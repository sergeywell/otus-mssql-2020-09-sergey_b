use [WideWorldImporters];
/*
1. � ������ �������� ���� ���� StockItems.xml.
��� ������ �� ������� Warehouse.StockItems.
������������� ��� ������ � ������� ������� � ������, ������������ Warehouse.StockItems.
����: StockItemName, SupplierID, UnitPackageID, OuterPackageID, QuantityPerOuter, TypicalWeightPerUnit, LeadTimeDays, IsChillerStock, TaxRate, UnitPrice

����������� - ���� �� ������� � insert, update, merge, �� ��������� ��� ������ � ������� Warehouse.StockItems.
������������ ������ � ������� ��������, ������������� �������� (������������ ������ �� ���� StockItemName).
*/

DECLARE @xml XML = (
SELECT * FROM OPENROWSET
(BULK 'C:\Users\bokkaradm\Desktop\otus-mssql-2020-09-sergey_b\HW6\StockItems.xml', SINGLE_BLOB) as data)

DECLARE @docHandle int
EXEC sp_xml_preparedocument @docHandle OUTPUT, @xml

SELECT *
into #tmp_xml
FROM OPENXML(@docHandle, N'/StockItems/Item')
WITH (StockItemName1 nvarchar(100) '@Name'
		,SupplierID1 int 'SupplierID'
		,UnitPackageID1 int 'Package/UnitPackageID'
		,OuterPackageID1 int 'Package/OuterPackageID'
		,QuantityPerOuter1 int 'Package/QuantityPerOuter'
		,TypicalWeightPerUnit1 decimal(18,3) 'Package/TypicalWeightPerUnit'
		,LeadTimeDays1 int 'LeadTimeDays'
		,IsChillerStock1 bit 'IsChillerStock'
		,TaxRate1 decimal(18,3) 'TaxRate'
		,UnitPrice1 decimal(18,2) 'UnitPrice');

UPDATE Warehouse.StockItems
SET SupplierID = x.SupplierID1,
	UnitPackageID = x.UnitPackageID1,
	OuterPackageID = x.OuterPackageID1,
	QuantityPerOuter = x.QuantityPerOuter1,
	TypicalWeightPerUnit = x.TypicalWeightPerUnit1,
	LeadTimeDays = x.LeadTimeDays1,
	IsChillerStock = x.IsChillerStock1,
	TaxRate = x.TaxRate1,
	UnitPrice = x.UnitPrice1
FROM #tmp_xml x
WHERE StockItemName = x.StockItemName1;

INSERT INTO Warehouse.StockItems (StockItemName, SupplierID, UnitPackageID, OuterPackageID, QuantityPerOuter, 
		TypicalWeightPerUnit, LeadTimeDays, IsChillerStock, TaxRate, UnitPrice, LastEditedBy)
SELECT StockItemName1, SupplierID1, UnitPackageID1, OuterPackageID1, QuantityPerOuter1, 
		TypicalWeightPerUnit1, LeadTimeDays1, IsChillerStock1, TaxRate1, UnitPrice1, 1
FROM #tmp_xml x
WHERE StockItemName1 NOT IN (SELECT StockItemName
							FROM Warehouse.StockItems);

DROP TABLE #tmp_xml;


/*
2. ��������� ������ �� ������� StockItems � ����� �� xml-����, ��� StockItems.xml

���������� � �������� 1, 2:
* ���� � ��������� � ���� ����� ��������, �� ����� ������� ������ SELECT c ����������� � ���� XML.
* ���� � ��� � ������� ������������ �������/������ � XML, �� ������ ����� ���� XML � ���� �������.
* ���� � ���� XML ��� ����� ������, �� ������ ����� ����� �������� ������ � ������������� �� � ������� (��������, � https://data.gov.ru).
* ������ ��������/������� � ���� https://docs.microsoft.com/en-us/sql/relational-databases/import-export/examples-of-bulk-import-and-export-of-xml-documents-sql-server
*/

SELECT TOP 15
	StockItemName as [@Name]
	,SupplierID as [SupplierID]
	,UnitPackageID as [Package/UnitPackageID]
	,OuterPackageID as [Package/OuterPackageID]
	,QuantityPerOuter as [Package/QuantityPerOuter]
	,TypicalWeightPerUnit as [Package/TypicalWeightPerUnit]
	,LeadTimeDays as [LeadTimeDays]
	,IsChillerStock as [IsChillerStock]
	,TaxRate as [TaxRate]
	,UnitPrice as [UnitPrice]
FROM [Warehouse].[StockItems]
FOR XML PATH('Item'), ROOT('StockItema')
GO


/* Variant with Python - not tested
exec sp_execute_external_script 
@language =N'Python',
@script=N'file = open("StockItems2.xml", "w")
file.write(str({0}))
file.close()
', 
@input_data_1 = N'SELECT TOP 15
	StockItemName as [@Name]
	,SupplierID as [SupplierID]
	,UnitPackageID as [Package/UnitPackageID]
	,OuterPackageID as [Package/OuterPackageID]
	,QuantityPerOuter as [Package/QuantityPerOuter]
	,TypicalWeightPerUnit as [Package/TypicalWeightPerUnit]
	,LeadTimeDays as [LeadTimeDays]
	,IsChillerStock as [IsChillerStock]
	,TaxRate as [TaxRate]
	,UnitPrice as [UnitPrice]
FROM [Warehouse].[StockItems]
FOR XML PATH("Item"), ROOT("StockItema")'
*/



/*
3. � ������� Warehouse.StockItems � ������� CustomFields ���� ������ � JSON.
�������� SELECT ��� ������:
- StockItemID
- StockItemName
- CountryOfManufacture (�� CustomFields)
- FirstTag (�� ���� CustomFields, ������ �������� �� ������� Tags)
*/

SELECT StockItemID, StockItemName, CustomFields, 
	JSON_VALUE(CustomFields, '$.CountryOfManufacture') as CountryOfManufacture,
	JSON_VALUE(CustomFields, '$.Tags[0]') as FirstTag
FROM Warehouse.StockItems;



/*
4. ����� � StockItems ������, ��� ���� ��� "Vintage".
�������:
- StockItemID
- StockItemName
- (�����������) ��� ���� (�� CustomFields) ����� ������� � ����� ����

���� ������ � ���� CustomFields, � �� � Tags.
������ �������� ����� ������� ������ � JSON.
��� ������ ������������ ���������, ������������ LIKE ���������.

������ ���� � ����� ����:
... where ... = 'Vintage'

��� ������� �� �����:
... where ... Tags like '%Vintage%'
... where ... CustomFields like '%Vintage%'
*/

WITH A as (
	SELECT si.StockItemID, si.StockItemName, 
	Tags.value,
	Tags 
	FROM Warehouse.StockItems si
	CROSS APPLY OPENJSON(CustomFields, '$.Tags')  Tags
	WHERE Tags.value = 'Vintage'
)
SELECT StockItemID, StockItemName, Value, 
		REPLACE(REPLACE (REPLACE(Tags, '[', ''), ']', ''), '"', '') as Tags
FROM A