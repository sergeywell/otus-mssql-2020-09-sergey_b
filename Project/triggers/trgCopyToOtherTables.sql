USE [dbPurgatory]
GO

/****** Object:  Trigger [dbo].[trgCopyToOtherTables]    Script Date: 08.02.2021 14:11:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[trgCopyToOtherTables] 
   ON  [dbo].[auth_user_groups]
   AFTER INSERT
AS 
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
		SET NOCOUNT ON;
		
		DECLARE @groupid int 
		SELECT @groupid = ug.group_id
		FROM [inserted] ug
	
		IF @groupid = 1
		BEGIN
			INSERT INTO [dbo].[tbHR]
			SELECT ug.user_id AS [Id]
				  ,u.[first_name] AS [FirstName]
				  ,u.[last_name] AS [SecondName]
				  ,'' AS [MiddleName]
				  ,ISNULL(c.[Id], 0) AS [CompanyId] -- 0 � ������� tbCompany ������������� �������� unknown
			FROM [dbo].[auth_user] u
			RIGHT JOIN [inserted] /*[dbo].[auth_user_groups]*/ ug ON u.id = ug.user_id
			LEFT JOIN [dbo].[tbCompany] c ON RIGHT(u.[email], (LEN(u.[email])-CHARINDEX(N'@',u.[email]))) = c.[Suffix]
		END
		ELSE IF @groupid = 2
		BEGIN
			INSERT INTO [dbo].[tbSD]
			SELECT ug.user_id AS [Id]
				  ,u.[first_name] AS [FirstName]
				  ,u.[last_name] AS [SecondName]
				  ,ISNULL(c.[Id], 0) AS [CompanyId] -- 0 � ������� tbCompany ������������� �������� unknown
			FROM [dbo].[auth_user] u
			RIGHT JOIN [inserted] /*[dbo].[auth_user_groups]*/ ug ON u.id = ug.user_id
			LEFT JOIN [dbo].[tbCompany] c ON RIGHT(u.[email], (LEN(u.[email])-CHARINDEX(N'@',u.[email]))) = c.[Suffix]
		END
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		
		INSERT INTO [dbo].[tbErrorLog]
		SELECT GETDATE(), ERROR_MESSAGE()
		
		BEGIN TRANSACTION
	END CATCH
END
GO

ALTER TABLE [dbo].[auth_user_groups] ENABLE TRIGGER [trgCopyToOtherTables]
GO


