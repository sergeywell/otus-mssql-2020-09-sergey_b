USE [dbPurgatory]
GO

/****** Object:  Trigger [dbo].[trgErrorMail]    Script Date: 08.02.2021 14:10:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[trgErrorMail] 
   ON  [dbo].[tbErrorLog]
   AFTER INSERT
AS 
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
		SET NOCOUNT ON;

		DECLARE @bodymail nvarchar(4000)

		SELECT @bodymail = '������ � dbPurgatory: ' + ErrorDesc
		FROM /*[dbo].[tbErrorLog]*/[inserted] e
		
		exec msdb.dbo.sp_send_dbmail @profile_name = 'test@mail.com', 
									 @recipients = 'test@mail.com', 
									 @subject = N'������ � ���������', 
									 @body = @bodymail, 
									 @body_format = 'text'
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION 
		BEGIN TRANSACTION
	END CATCH
END
GO

ALTER TABLE [dbo].[tbErrorLog] ENABLE TRIGGER [trgErrorMail]
GO


