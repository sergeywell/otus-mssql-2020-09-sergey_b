USE [dbPurgatory]
GO

/****** Object:  Trigger [dbo].[trgLog_Mail]    Script Date: 08.02.2021 14:10:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================
-- Sergey Bokkar
-- 07/12/20
-- ================================================
CREATE TRIGGER [dbo].[trgLog_Mail] 
   ON  [dbo].[tbData]
   AFTER INSERT, UPDATE
AS 
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
		SET NOCOUNT ON;
----------------- LOG -------------------------------------------------------------------------------------

		DECLARE @timestamp datetime2(7) = getdate()

		INSERT INTO [dbo].[tbLogDataChanges] ([ChData],[ChVersion],[Id],[INN],[CompanyId],[EmploymentDate],[DismissalDate]
													,[Notes],[AuthorId],[Position],[ReasonId],[Created],[DLM], [ChUser])
		SELECT @timestamp, 'old', [Id],[INN],[CompanyId],[EmploymentDate],[DismissalDate]
								,[Notes],[AuthorId],[Position],[ReasonId],[Created],[DLM],system_user
		FROM [deleted]


		INSERT INTO [dbo].[tbLogDataChanges] ([ChData],[ChVersion],[Id],[INN],[CompanyId],[EmploymentDate],[DismissalDate]
													,[Notes],[AuthorId],[Position],[ReasonId],[Created],[DLM],[ChUser])
		SELECT @timestamp, 'new', [Id],[INN],[CompanyId],[EmploymentDate],[DismissalDate]
								,[Notes],[AuthorId],[Position],[ReasonId],[Created],[DLM],system_user
		FROM [inserted]

---------------- MAIL -----------------------------------------------------------------------------------------
		
		DECLARE @inn bigint, @CompanyId bigint
		DECLARE @bodymail nvarchar(150), @recipient nvarchar(150)

		SELECT @inn = INN, @recipient = 'test@mail.com' , @CompanyId = d.CompanyId --+c.Suffix
		FROM /*[dbo].[tbData]*/[inserted] d
		LEFT JOIN [dbo].[tbCompany] c ON d.CompanyId = c.Id
		
		IF @CompanyId NOT IN ('0', '1')
		BEGIN
		SELECT @bodymail = N'���� �������m�, ������ ��������� ��� � ��������� ���������/����������! �������� ���������? ������� - ���: ' + CAST(@inn AS nvarchar(14))
		END
		ELSE
		BEGIN
		SELECT @recipient = 'test@mail.com',
			   @bodymail = '������, ���� ���������� � ���� �������! ���������� �� �������� - ���: ' + CAST(@inn AS nvarchar(14))
		END
		
			exec msdb.dbo.sp_send_dbmail @profile_name = 'test@mail.com', 
										 @recipients = @recipient, 
										 @subject = N'��������� � ���������', 
										 @body = @bodymail, 
										 @body_format = 'text'
---------------------------------------------------------------------------------------------------------
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION 

		INSERT INTO [dbo].[tbErrorLog]
		SELECT GETDATE(), ERROR_MESSAGE()

		BEGIN TRANSACTION
	END CATCH
END
GO

ALTER TABLE [dbo].[tbData] ENABLE TRIGGER [trgLog_Mail]
GO


