USE [WideWorldImporters];
/*1. ��������� �������� ������, ������� � ���������� ������ ���������� ��������� ������� ���������� ����:
�������� �������
�������� ���������� �������

�������� ����� � ID 2-6, ��� ��� ������������� Tailspin Toys
--��� ������� ����� �������� ��� ����� �������� ������ ���������
--�������� �������� Tailspin Toys (Gasport, NY) - �� �������� � ����� ������ Gasport,NY
���� ������ ����� ������ dd.mm.yyyy �������� 25.12.2019

��������, ��� ������ ��������� ����������:
InvoiceMonth Peeples Valley, AZ Medicine Lodge, KS Gasport, NY Sylvanite, MT Jessie, ND
01.01.2013 3 1 4 2 2
01.02.2013 7 3 4 2 1
*/

SELECT * 
FROM (
	SELECT 
		SUBSTRING (CustomerName, CHARINDEX('(', CustomerName)+1, CHARINDEX(')', 
			CustomerName)-CHARINDEX('(', CustomerName)-1)  as ShortCustomerName
		,FORMAT(o.OrderDate, 'dd.MM.yy') as OrderDate
		,o.OrderID
	FROM [Sales].[Customers] cus
	LEFT JOIN [Sales].[Orders] o on o.CustomerID = cus.CustomerID
	WHERE cus.CustomerId in (2,3,4,5,6)
) as A
PIVOT (
	COUNT(OrderId)
	FOR ShortCustomerName IN ([Gasport, NY], [Jessie, ND], [Medicine Lodge, KS], [Peeples Valley, AZ], [Sylvanite, MT])
) as PVT
ORDER BY OrderDate;


/*
2. ��� ���� �������� � ������, � ������� ���� Tailspin Toys
������� ��� ������, ������� ���� � �������, � ����� �������

������ �����������
CustomerName AddressLine
Tailspin Toys (Head Office) Shop 38
Tailspin Toys (Head Office) 1877 Mittal Road
Tailspin Toys (Head Office) PO Box 8975
Tailspin Toys (Head Office) Ribeiroville
.....
*/
SELECT CustomerName, AddressLine
FROM (
	SELECT CustomerName, DeliveryAddressLine1, DeliveryAddressLine2
	FROM [Sales].[Customers] cus
	WHERE CustomerName like 'Tailspin Toys%'
	) as A
UNPIVOT (AddressLine FOR DeliveryAddress IN (DeliveryAddressLine1, DeliveryAddressLine2)
) as UNP;

/*
3. � ������� ����� ���� ���� � ����� ������ �������� � ���������
�������� ������� �� ������, ��������, ��� - ����� � ���� ��� ���� �������� ���� ��������� ���
������ ������

CountryId CountryName Code
1 Afghanistan AFG
1 Afghanistan 4
3 Albania ALB
3 Albania 8
*/

SELECT CountryId, CountryName, Code
FROM (
	SELECT CountryID, CountryName, IsoAlpha3Code, 
			CAST(IsoNumericCode as nvarchar(3))  as IsoNumericCode
	FROM [Application].[Countries]
	) as A
UNPIVOT(Code FOR AlpNumCode IN (IsoAlpha3Code, IsoNumericCode)
) as UNP;


/*
4. �������� �� ������� ������� 2 ����� ������� ������, ������� �� �������
� ����������� ������ ���� �� ������, ��� ��������, �� ������, ����, ���� �������
*/

SELECT cus.CustomerID, cus.CustomerName, inv.StockItemID, inv.UnitPrice, inv.InvoiceDate
FROM [Sales].[Customers] cus
CROSS APPLY ( SELECT TOP 2 StockItemID, UnitPrice, InvoiceDate
			   FROM [Sales].[Invoices] i
			   LEFT JOIN [Sales].[InvoiceLines] il on i.InvoiceID = il.InvoiceID
			   WHERE i.CustomerID = cus.CustomerID
			   ORDER BY il.UnitPrice DESC
		 ) as inv;

/*
5. Code review (�����������). ������ �������� � ��������� Hometask_code_review.sql.
��� ������ ������?
��� ����� �������� CROSS APPLY - ����� �� ������������ ������ ��������� �������\�������?
*/
-- ���� ��� ������� �� �������, 
-- ��� �������� ������.
