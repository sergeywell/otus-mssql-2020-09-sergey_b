USE WideWorldImporters
GO

/* 
1. �������� ����������� (Application.People), ������� �������� ������������ (IsSalesPerson), 
� �� ������� �� ����� ������� 04 ���� 2015 ����. ������� �� ���������� � ��� ������ ���. 
������� �������� � ������� Sales.Invoices.
*/
SELECT PersonID, FullName
FROM Application.People
WHERE IsSalesperson = 1 and PersonID not in (
	SELECT distinct SalespersonPersonID
	FROM Sales.Invoices
	WHERE InvoiceDate = '20150704'
)

GO
----------------------------------------------------
WITH a as (
	SELECT distinct SalespersonPersonID
	FROM Sales.Invoices
	WHERE InvoiceDate = '20150704'
)

SELECT PersonID, FullName
FROM Application.People p
LEFT JOIN a on a.SalespersonPersonID = p.PersonID
WHERE IsSalesperson = 1  and a.SalespersonPersonID is null;


/* 
2. �������� ������ � ����������� ����� (�����������). �������� ��� �������� ����������. 
	�������: �� ������, ������������ ������, ����.
*/

SELECT StockItemID, StockItemName, UnitPrice
FROM [Warehouse].[StockItems]
WHERE UnitPrice = (SELECT MIN(UnitPrice)
			FROM [Warehouse].[StockItems]);

SELECT TOP 1 si.StockItemID, si.StockItemName, mp.MinPrice
FROM [Warehouse].[StockItems]si
INNER JOIN (SELECT StockItemID, MIN(UnitPrice) as MinPrice
			FROM [Warehouse].[StockItems]
			GROUP BY StockItemID
			) mp on si.StockItemID = mp.StockItemID and si.UnitPrice = mp.MinPrice
order by MinPrice;


/* 
3. �������� ���������� �� ��������, ������� �������� �������� ���� ������������ �������� �� Sales.CustomerTransactions. 
	����������� ��������� �������� (� ��� ����� � CTE).
*/

SELECT TOP 5 c.*
FROM Sales.CustomerTransactions ct
LEFT JOIN Sales.Customers c on c.CustomerID = ct.CustomerID
ORDER BY TransactionAmount DESC;

WITH A as (
	SELECT TOP 5 CustomerID
	FROM Sales.CustomerTransactions ct
	ORDER BY TransactionAmount DESC
)

SELECT  *
FROM Sales.Customers 
WHERE CustomerID in (SELECT CustomerID from A)


/* 
4. �������� ������ (�� � ��������), � ������� ���� ���������� ������, �������� � ������ ����� ������� �������, 
	� ����� ��� ����������, ������� ����������� �������� ������� (PackedByPersonID).
*/

WITH A as (
	SELECT DISTINCT TOP 3 StockItemID, UnitPrice
	FROM [Sales].[OrderLines]
	ORDER BY UnitPrice DESC)

SELECT c.CityID, c.CityName, p.FullName
FROM [Sales].[Invoices] i
LEFT JOIN [Sales].[InvoiceLines] il on i.InvoiceID = il.InvoiceID
LEFT JOIN [Sales].[Customers] cus on i.CustomerID = cus.CustomerID
LEFT JOIN [Application].Cities c on cus.DeliveryCityID = c.CityID
LEFT JOIN [Application].[People] p on i.PackedByPersonID = p.PersonID
WHERE il.StockItemId in (select StockItemID FROM A );


/*
�����������:

5. ���������, ��� ������ � ������������� ������:
SELECT
Invoices.InvoiceID,
Invoices.InvoiceDate,
(SELECT People.FullName
FROM Application.People
WHERE People.PersonID = Invoices.SalespersonPersonID
) AS SalesPersonName,
SalesTotals.TotalSumm AS TotalSummByInvoice,
(SELECT SUM(OrderLines.PickedQuantity*OrderLines.UnitPrice)
FROM Sales.OrderLines
WHERE OrderLines.OrderId = (SELECT Orders.OrderId
FROM Sales.Orders
WHERE Orders.PickingCompletedWhen IS NOT NULL
AND Orders.OrderId = Invoices.OrderId)
) AS TotalSummForPickedItems
FROM Sales.Invoices
JOIN
(SELECT InvoiceId, SUM(Quantity*UnitPrice) AS TotalSumm
FROM Sales.InvoiceLines
GROUP BY InvoiceId
HAVING SUM(Quantity*UnitPrice) > 27000) AS SalesTotals
ON Invoices.InvoiceID = SalesTotals.InvoiceID
ORDER BY TotalSumm DESC

����� ��������� ��� � ������� ��������� ������������� �������, ��� � � ������� ��������� �����\���������. 
�������� ������������������ �������� ����� ����� SET STATISTICS IO, TIME ON. ���� ������� � ������� ��������, 
�� ����������� �� (����� � ������� ����� ��������� �����). �������� ���� ����������� �� ������ �����������.
*/

--- �������� � ����� ���������������, ��� �������� ���������� ������.
--- ���������� ������ ��� ���������� ������������ ����� ����� ����. 
SELECT
	Invoices.InvoiceID,
	Invoices.InvoiceDate,
	People.FullName as SalesPersonName,
	SalesTotals.TotalSumm AS TotalSummByInvoice,
	PickedTotal.TotalPicked	AS TotalSummForPickedItems
FROM Sales.Invoices
LEFT JOIN Application.People on People.PersonID = Invoices.SalespersonPersonID
JOIN (	SELECT InvoiceId, SUM(Quantity*UnitPrice) AS TotalSumm
		FROM Sales.InvoiceLines
		GROUP BY InvoiceId
		HAVING SUM(Quantity*UnitPrice) > 27000
	) AS SalesTotals ON Invoices.InvoiceID = SalesTotals.InvoiceID
JOIN (SELECT OrderID, SUM(OrderLines.PickedQuantity*OrderLines.UnitPrice) as TotalPicked
		FROM Sales.OrderLines
		WHERE OrderLines.OrderId in (SELECT Orders.OrderId
									FROM Sales.Orders
									WHERE Orders.PickingCompletedWhen IS NOT NULL
									) 
		GROUP BY OrderID
		) as PickedTotal on Invoices.OrderID = PickedTotal.OrderID

ORDER BY TotalSumm DESC;

-----------------------------------------------------------------------------------------------

WITH A as (
		SELECT InvoiceId, SUM(Quantity*UnitPrice) AS TotalSumm
		FROM Sales.InvoiceLines
		GROUP BY InvoiceId
		HAVING SUM(Quantity*UnitPrice) > 27000
), 

B as (
	SELECT OrderID, SUM(OrderLines.PickedQuantity*OrderLines.UnitPrice) as TotalPicked
		FROM Sales.OrderLines
		WHERE OrderLines.OrderId in (SELECT Orders.OrderId
									FROM Sales.Orders
									WHERE Orders.PickingCompletedWhen IS NOT NULL
									) 
		GROUP BY OrderID
) 

SELECT
	Invoices.InvoiceID,
	Invoices.InvoiceDate,
	People.FullName as SalesPersonName,
	SalesTotals.TotalSumm AS TotalSummByInvoice,
	PickedTotal.TotalPicked	AS TotalSummForPickedItems
FROM Sales.Invoices
LEFT JOIN Application.People on People.PersonID = Invoices.SalespersonPersonID
JOIN A AS SalesTotals ON Invoices.InvoiceID = SalesTotals.InvoiceID
JOIN B as PickedTotal ON Invoices.OrderID = PickedTotal.OrderID
ORDER BY TotalSumm DESC


/*
5. � ���������� � �������� ���� ���� HT_reviewBigCTE.sql - �������� ���� ������ � ��������, 
��� �� ������ ������� � � ��� ��� �����. ���� ���� ���� �� ��������� �������, �� �������� ��.
*/

-- ������ �� �� ������, ������ ������ �������������� ������ ��� �������� � ��������� �� � ���.
-- ������� ���������� �� ��� ����