USE [WideWorldImporters]
Go

/*
0. �������� �������� ��������� ������������ ������� � ��������� ������� ������ �������.
*/
--CREATE PROCEDURE dbo.spGetTopClientByOrder
--AS
--BEGIN
--	WITH A as (
--		SELECT OrderId, SUM(Quantity*UnitPrice) as Total
--		FROM [WideWorldImporters].[Sales].[OrderLines]
--		GROUP BY OrderId
--	)
--	SELECT TOP 1 c.CustomerID, c.CustomerName, A.Total, o.OrderDate
--	FROM [Sales].[Customers] c
--	INNER JOIN [Sales].[Orders] o on o.CustomerID = c.CustomerID
--	INNER JOIN A on A.OrderId = o.OrderID
--	ORDER BY A.Total desc 
--END;

CREATE PROCEDURE dbo.spGetTopClientByOrder
AS
BEGIN
	SELECT TOP 1 c.CustomerID, c.CustomerName, A.Total, o.OrderDate
	FROM [Sales].[Customers] c
	INNER JOIN [Sales].[Orders] o on o.CustomerID = c.CustomerID
	INNER JOIN (		SELECT OrderId, SUM(Quantity*UnitPrice) as Total
		FROM [WideWorldImporters].[Sales].[OrderLines]
		GROUP BY OrderId) as A on A.OrderId = o.OrderID
	ORDER BY A.Total desc
END;



EXEC  dbo.spGetTopClientByOrder;

GO
/*
1) �������� ������� ������������ ������� � ���������� ������ �������.
*/

CREATE FUNCTION dbo.fnGetTopClientByOrder()
RETURNS TABLE
AS
RETURN (
	SELECT TOP 1 c.CustomerID, c.CustomerName, A.Total, o.OrderDate
	FROM [Sales].[Customers] c
	INNER JOIN [Sales].[Orders] o on o.CustomerID = c.CustomerID
	INNER JOIN (		SELECT OrderId, SUM(Quantity*UnitPrice) as Total
		FROM [WideWorldImporters].[Sales].[OrderLines]
		GROUP BY OrderId) as A on A.OrderId = o.OrderID
	ORDER BY A.Total desc
);

SELECT * FROM dbo.fnGetTopClientByOrder();

GO

/*2) �������� �������� ��������� � �������� ���������� �ustomerID, ��������� ����� ������� �� ����� �������.
������������ ������� :
Sales.Customers
Sales.Invoices
Sales.InvoiceLines
*/

CREATE PROCEDURE dbo.spGetTotalSumByClient (@CustId int)
AS
BEGIN
	SELECT SUM(Quantity*UnitPrice) as TotalSum
	FROM [Sales].[Customers] c
	INNER JOIN [Sales].[Invoices] i on i.CustomerID = c.CustomerID
	INNER JOIN [Sales].[InvoiceLines] il on il.InvoiceId = i.InvoiceID
	WHERE c.CustomerID = @CustId
END;

EXEC  dbo.spGetTotalSumByClient 25;

GO


/*
3) ������� ���������� ������� � �������� ���������, ���������� � ��� ������� � ������������������ � ������.
*/
-- Task 0 & 1

EXEC  dbo.spGetTopClientByOrder;
SELECT * FROM dbo.fnGetTopClientByOrder();

-- ����� ���������� ��������� ����������, ��������� �������� ����������. �� ������������������ ����� �� ������.


/*
4) �������� ��������� ������� �������� ��� �� ����� ������� ��� ������ ������ result set'� ��� ������������� �����.
*/

CREATE FUNCTION dbo.fnGetTotalSumByClient(@CustId int)
RETURNS TABLE
AS
RETURN (
	SELECT c.CustomerId, SUM(Quantity*UnitPrice) as TotalSum
	FROM [Sales].[Customers] c
	INNER JOIN [Sales].[Invoices] i on i.CustomerID = c.CustomerID
	INNER JOIN [Sales].[InvoiceLines] il on il.InvoiceId = i.InvoiceID
	WHERE c.CustomerID = @CustId
	GROUP BY c.CustomerId
);



SELECT c.CustomerName, ts.*
FROM Sales.Customers c
CROSS APPLY  dbo.fnGetTotalSumByClient(c.CustomerID) ts
ORDER BY c.CustomerName;


/*
�� ���� ����������, � �������� ������� ��� ��������������
5) ����� ������� �������� ����� � ������.
*/

-- ��� ��������� � ������� ��������� SELECT � ��� ��� ���������� ���������� ������ ��������



