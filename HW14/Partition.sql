-- [Purchasing].[PurchaseOrders] + [Purchasing].[PurchaseOrderLines]

-- Create schema
CREATE PARTITION FUNCTION [fnYearPartition](DATE) AS RANGE RIGHT FOR VALUES
('20120101','20130101','20140101','20150101','20160101', '20170101',
 '20180101', '20190101', '20200101', '20210101');																																																									
GO

-- Create Function
CREATE PARTITION SCHEME [schmYearPartition] AS PARTITION [fnYearPartition] 
ALL TO ([PRIMARY])
GO

-- Create Tables
CREATE TABLE [Purchasing].[PurchaseOrdersPart](
	[PurchaseOrderID] [int] NOT NULL,
	[SupplierID] [int] NOT NULL,
	[OrderDate] [date] NOT NULL,
	[DeliveryMethodID] [int] NOT NULL,
	[ContactPersonID] [int] NOT NULL,
	[ExpectedDeliveryDate] [date] NULL,
	[SupplierReference] [nvarchar](20) NULL,
	[IsOrderFinalized] [bit] NOT NULL,
	[Comments] [nvarchar](max) NULL,
	[InternalComments] [nvarchar](max) NULL,
	[LastEditedBy] [int] NOT NULL,
	[LastEditedWhen] [datetime2](7) NOT NULL,
 ) ON [USERDATA] TEXTIMAGE_ON [USERDATA]
GO

CREATE TABLE [Purchasing].[PurchaseOrderLinesPart](
	[PurchaseOrderLineID] [int] NOT NULL,
	[PurchaseOrderID] [int] NOT NULL,
	[OrderDate] [date] NOT NULL,
	[StockItemID] [int] NOT NULL,
	[OrderedOuters] [int] NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[ReceivedOuters] [int] NOT NULL,
	[PackageTypeID] [int] NOT NULL,
	[ExpectedUnitPricePerOuter] [decimal](18, 2) NULL,
	[LastReceiptDate] [date] NULL,
	[IsOrderLineFinalized] [bit] NOT NULL,
	[LastEditedBy] [int] NOT NULL,
	[LastEditedWhen] [datetime2](7) NOT NULL,
) ON [USERDATA]
GO

ALTER TABLE [Purchasing].[PurchaseOrderLinesPart] ADD CONSTRAINT PK_Purchasing_PurchaseOrderLinesPart
PRIMARY KEY CLUSTERED  (OrderDate, PurchaseOrderId, PurchaseOrderLineId)
 ON [schmYearPartition]([OrderDate]);

ALTER TABLE [Purchasing].[PurchaseOrdersPart] ADD CONSTRAINT PK_Purchasing_PurchaseOrdersPart 
PRIMARY KEY CLUSTERED  (OrderDate, PurchaseOrderId)
 ON [schmYearPartition]([OrderDate]);

-- Insert data

 insert into  [Purchasing].[PurchaseOrdersPart]
 select * from  [Purchasing].[PurchaseOrders];

 insert into [Purchasing].[PurchaseOrderLinesPart] ([PurchaseOrderLineID]
      ,[PurchaseOrderID]
	  ,[OrderDate]
      ,[StockItemID]
      ,[OrderedOuters]
      ,[Description]
      ,[ReceivedOuters]
      ,[PackageTypeID]
      ,[ExpectedUnitPricePerOuter]
      ,[LastReceiptDate]
      ,[IsOrderLineFinalized]
      ,[LastEditedBy]
      ,[LastEditedWhen])

select pol.[PurchaseOrderLineID]
      ,pol.[PurchaseOrderID]
	  ,po.[OrderDate]
      ,pol.[StockItemID]
      ,pol.[OrderedOuters]
      ,pol.[Description]
      ,pol.[ReceivedOuters]
      ,pol.[PackageTypeID]
      ,pol.[ExpectedUnitPricePerOuter]
      ,pol.[LastReceiptDate]
      ,pol.[IsOrderLineFinalized]
      ,pol.[LastEditedBy]
      ,pol.[LastEditedWhen]
FROM [Purchasing].[PurchaseOrderLines] pol
JOIN [Purchasing].[PurchaseOrders] po on pol.PurchaseOrderID = po.PurchaseOrderID

--Result
SELECT  $PARTITION.fnYearPartition(OrderDate) AS Partition
		, COUNT(*) AS [COUNT]
		, MIN(OrderDate)
		, MAX(OrderDate) 
FROM [Purchasing].[PurchaseOrders]
GROUP BY $PARTITION.fnYearPartition(OrderDate) 
ORDER BY Partition ;  