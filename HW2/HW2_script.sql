USE WideWorldImporters
GO

/* 
1. ��� ������, � �������� ������� ���� "urgent" ��� �������� ���������� � "Animal". �������: �� ������, ������������ ������.
�������: Warehouse.StockItems. 
*/ 
SELECT StockItemID, StockItemName
FROM Warehouse.StockItems
WHERE StockItemName like '%urgent%' or StockItemName like 'Animal%'


/*
2. ����������� (Suppliers), � ������� �� ���� ������� �� ������ ������ (PurchaseOrders). ������� ����� JOIN, � ����������� ������� ������� �� �����. �������: �� ����������, ������������ ����������.
�������: Purchasing.Suppliers, Purchasing.PurchaseOrders.
*/

SELECT s.SupplierID, s.SupplierName 
FROM Purchasing.Suppliers s
LEFT JOIN Purchasing.PurchaseOrders po on po.SupplierID = s.SupplierID
WHERE po.PurchaseOrderID is null


/*
3. ������ (Orders) � ����� ������ ����� 100$ ���� ����������� ������ ������ ����� 20 ���� � 
�������������� ����� ������������ ����� ������ (PickingCompletedWhen).
�������:
* OrderID
* ���� ������ � ������� ��.��.����
* �������� ������, � ������� ���� �������
* ����� ��������, � �������� ��������� �������
* ����� ����, � ������� ��������� ���� ������� (������ ����� �� 4 ������)
* ��� ��������� (Customer)
�������� ������� ����� ������� � ������������ ��������, ��������� ������ 1000 � ��������� ��������� 100 �������. ���������� ������ ���� �� ������ ��������, ����� ����, ���� ������ (����� �� �����������).
�������: Sales.Orders, Sales.OrderLines, Sales.Customers.
*/

--SELECT o.OrderId,
--		FORMAT(o.OrderDate, 'dd.MM.yy') as OrderDate, 
--		FORMAT(o.OrderDate, 'MMMM') as OrderMonth,
--		DATEPART(QUARTER, o.OrderDate) as OrderQuarter,
--		CASE 
--			WHEN MONTH(o.OrderDate) <= 4 THEN 1
--			WHEN MONTH(o.OrderDate) between 5 and 8 THEN 2
--			WHEN MONTH(o.OrderDate) > 8 THEN 3
--		END as OrderThird,
--		c.CustomerName, 
--		*
--FROM Sales.Orders o
--LEFT JOIN Sales.OrderLines ol on o.OrderID = ol.OrderID
--LEFT JOIN Sales.Customers c on o.CustomerID = c.CustomerID
--where ol.PickingCompletedWhen is not null and (ol.UnitPrice > 100 or ol.Quantity > 20)


--SELECT Position, OrderId,OrderDate,OrderMonth,OrderQuarter,OrderThird,CustomerName
--FROM (SELECT o.OrderId,
--		ROW_NUMBER() OVER(ORDER BY  o.OrderDate ) as Position,
--		FORMAT(o.OrderDate, 'dd.MM.yy') as OrderDate, 
--		FORMAT(o.OrderDate, 'MMMM') as OrderMonth,
--		DATEPART(QUARTER, o.OrderDate) as OrderQuarter,
--		CASE 
--			WHEN MONTH(o.OrderDate) <= 4 THEN 1
--			WHEN MONTH(o.OrderDate) between 5 and 8 THEN 2
--			WHEN MONTH(o.OrderDate) > 8 THEN 3
--		END as OrderThird,
--		c.CustomerName
--	FROM Sales.Orders o
--	LEFT JOIN Sales.OrderLines ol on o.OrderID = ol.OrderID
--	LEFT JOIN Sales.Customers c on o.CustomerID = c.CustomerID
--	where ol.PickingCompletedWhen is not null and (ol.UnitPrice > 100 or ol.Quantity > 20)) a
--WHERE a.Position between 1001 and 1101


SELECT Position, OrderId,OrderDate,OrderMonth,OrderQuarter,OrderThird,CustomerName
FROM (SELECT o.OrderId,
		ROW_NUMBER() OVER(ORDER BY  o.OrderDate ) as Position,
		FORMAT(o.OrderDate, 'dd.MM.yy') as OrderDate, 
		FORMAT(o.OrderDate, 'MMMM') as OrderMonth,
		DATEPART(QUARTER, o.OrderDate) as OrderQuarter,
		CASE 
			WHEN MONTH(o.OrderDate) <= 4 THEN 1
			WHEN MONTH(o.OrderDate) between 5 and 8 THEN 2
			WHEN MONTH(o.OrderDate) > 8 THEN 3
		END as OrderThird,
		c.CustomerName
	FROM Sales.Orders o
	LEFT JOIN Sales.OrderLines ol on o.OrderID = ol.OrderID
	LEFT JOIN Sales.Customers c on o.CustomerID = c.CustomerID
	WHERE ol.PickingCompletedWhen is not null and (ol.UnitPrice > 100 or ol.Quantity > 20)
	) a
ORDER BY OrderQuarter
OFFSET 1000 ROWS FETCH FIRST 100 ROWS ONLY


/*
4. ������ ����������� (Purchasing.Suppliers), ������� ���� ��������� � ������ 2014 ���� � ��������� Air Freight ��� Refrigerated Air Freight (DeliveryMethodName).
�������:
* ������ �������� (DeliveryMethodName)
* ���� ��������
* ��� ����������
* ��� ����������� ���� ������������ ����� (ContactPerson)

�������: Purchasing.Suppliers, Purchasing.PurchaseOrders, Application.DeliveryMethods, Application.People.
*/

SELECT  DeliveryMethodName, ExpectedDeliveryDate, SupplierName, FullName as ContactPerson
FROM Purchasing.PurchaseOrders po
LEFT JOIN Purchasing.Suppliers s on po.SupplierID = s.SupplierID
LEFT JOIN Application.DeliveryMethods dm on po.DeliveryMethodId = dm.DeliveryMethodID
LEFT JOIN Application.People p on po.ContactPersonID = p.PersonID
WHERE po.ExpectedDeliveryDate between '20130101' and '20130131' and DeliveryMethodName in ('Air Freight', 'Refrigerated Air Freight')


/*
5. ������ ��������� ������ (�� ����) � ������ ������� � ������ ����������, ������� ������� ����� (SalespersonPerson).
*/
SELECT TOP 10 po.*, p.FullName as ContactPerson, p2.FullName as SalespersonPerson 
FROM Purchasing.PurchaseOrders po
LEFT JOIN Application.People p on po.ContactPersonID = p.PersonID
LEFT JOIN Purchasing.Suppliers s on po.SupplierID = s.SupplierID
LEFT JOIN Application.People p2 on s.PrimaryContactPersonID = p2.PersonID
ORDER BY po.OrderDate DESC


/*
6. ��� �� � ����� �������� � �� ���������� ��������, ������� �������� ����� Chocolate frogs 250g. ��� ������ �������� � Warehouse.StockItems.
*/
SELECT p.PersonID, p.FullName, p.PhoneNumber 
FROM Purchasing.PurchaseOrders po
LEFT JOIN Application.People p on po.ContactPersonID = p.PersonID
LEFT JOIN Purchasing.PurchaseOrderLines pol on po.PurchaseOrderID = pol.PurchaseOrderID
LEFT JOIN Warehouse.StockItems si on si.StockItemID = pol.StockItemID
WHERE si.StockItemName = 'Chocolate frogs 250g'


/*
7. ��������� ������� ���� ������, ����� ����� ������� �� �������
�������:
* ��� �������
* ����� �������
* ������� ���� �� ����� �� ���� �������
* ����� ����� ������
������� �������� � ������� Sales.Invoices � ��������� ��������.
*/

SELECT  YEAR(o.OrderDate) as [Year], MONTH(o.OrderDate) as [Month], 
	AVG(ol.UnitPrice) as [Averege], SUM(ol.Quantity*ol.UnitPrice) as [Summa] 
FROM Sales.Invoices i
LEFT JOIN Sales.Orders o on i.OrderID = o.OrderID
LEFT JOIN Sales.OrderLines ol on o.OrderID = ol.OrderID
group by YEAR(o.OrderDate), MONTH(o.OrderDate)



/*
8. ���������� ��� ������, ��� ����� ����� ������ ��������� 10 000
�������:
* ��� �������
* ����� �������
* ����� ����� ������
������� �������� � ������� Sales.Invoices � ��������� ��������.
*/

SELECT  YEAR(o.OrderDate) as [Year], MONTH(o.OrderDate) as [Month], 
	SUM(ol.Quantity*ol.UnitPrice) as [Summa] 
FROM Sales.Invoices i
LEFT JOIN Sales.Orders o on i.OrderID = o.OrderID
LEFT JOIN Sales.OrderLines ol on o.OrderID = ol.OrderID
GROUP BY YEAR(o.OrderDate), MONTH(o.OrderDate)
HAVING SUM(ol.Quantity*ol.UnitPrice) > 10000
ORDER BY Year, Month




/*
9. ������� ����� ������, ���� ������ ������� � ���������� ���������� �� �������, 
	�� �������, ������� ������� ����� 50 �� � �����.
����������� ������ ���� �� ����, ������, ������.
�������:
* ��� �������
* ����� �������
* ������������ ������
* ����� ������
* ���� ������ �������
* ���������� ����������
������� �������� � ������� Sales.Invoices � ��������� ��������.
*/

SELECT  YEAR(o.OrderDate) as [Year], MONTH(o.OrderDate) as [Month], ol.Description as ProductName,
	SUM(ol.Quantity*ol.UnitPrice) as [Summa], MIN(o.OrderDate) as [FirstSell],
	SUM(ol.Quantity) as [Qty]
FROM Sales.Invoices i
LEFT JOIN Sales.Orders o on i.OrderID = o.OrderID
LEFT JOIN Sales.OrderLines ol on o.OrderID = ol.OrderID
GROUP BY YEAR(o.OrderDate), MONTH(o.OrderDate), ol.Description
HAVING SUM(ol.Quantity) < 50
ORDER BY Year, Month


/*
�����������:
�������� ������� 8-9 ���, ����� ���� � �����-�� ������ �� ���� ������, �� ���� ����� ����� ����������� �� � �����������, �� ��� ���� ����.
*/